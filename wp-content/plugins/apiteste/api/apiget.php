<?php



function get_api($request){

	$paciente = get_user_by('id',$request['id']);
	$nomesdependentes = get_field('field_62cf40ce408b0','user_'.$request['id']);
	$metauser = get_user_meta($request['id']);
	// var_dump($metauser["first_name"][0]);
	// die();
	if($nomesdependentes){
		foreach($nomesdependentes as $key=>$dependente){		
			$dependentes[] = $nomesdependentes[$key];										
		}
   	} 

	$retornopaciente= array(
		"nome"=>$metauser["first_name"][0],
		'id'=>$paciente->ID,
		'sobrenome'=>$metauser["last_name"][0],
		'genero'=> get_field('field_62cf3f62cc8f0','user_'.$request['id']),
		'dt_nascimento'=>get_field('field_62d0b7073e67a','user_'.$request['id']),
		'telefone'=> get_field('field_62d203fc277ee','user_'.$request['id']),
		'dependentes'=>$dependentes
	);

	return rest_ensure_response($retornopaciente);
   
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'api', '/paciente/(?P<id>[0-9]+)', array(
	  'methods' => WP_REST_Server::READABLE,
	  'callback' => 'get_api',
	) );
} );