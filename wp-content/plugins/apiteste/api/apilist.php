<?php



function list_api(){
	
    $pacientes = get_users( array( 'role__in' => array( 'paciente') ) );
	$retornopacientes = array();
	foreach ( $pacientes as $paciente ) {
		$retornopacientes[]= array("nome"=>$paciente->first_name,'id'=>$paciente->ID);		
	}

	return rest_ensure_response($retornopacientes);
   
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'api', '/getpacientes', array(
	  'methods' => WP_REST_Server::READABLE,
	  'callback' => 'list_api',
	) );
} );