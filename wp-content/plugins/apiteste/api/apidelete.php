<?php


require_once(ABSPATH.'wp-admin/includes/user.php');
function delete_api($request){
 
    wp_delete_user($request['id']);
	return rest_ensure_response(true);
   
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'api', '/paciente/(?P<id>[0-9]+)', array(
	  'methods' => WP_REST_Server::DELETABLE,
	  'callback' => 'delete_api',
	) );
} );