<?php
/*
Plugin Name: Testeapi

*/

$dirbase = plugin_dir_path( __FILE__ );


function jss() {   
    wp_enqueue_script( 'list', plugin_dir_url( __FILE__ ) . 'apijs/list.js', array('jquery'), '', true );
    wp_enqueue_script( 'geral', plugin_dir_url( __FILE__ ) . 'apijs/geral.js', array('jquery'), '', true );
    wp_enqueue_script( 'get', plugin_dir_url( __FILE__ ) . 'apijs/get.js', array('jquery'), '', true );
    wp_enqueue_script( 'delete', plugin_dir_url( __FILE__ ) . 'apijs/delete.js', array('jquery'), '', true );
  
    wp_enqueue_script( 'post', plugin_dir_url( __FILE__ ) . 'apijs/post.js', array('jquery'), '', true );
    wp_enqueue_script( 'edit', plugin_dir_url( __FILE__ ) . 'apijs/edit.js' , array('jquery'), '', true);
}
add_action('wp_enqueue_scripts', 'jss');

require_once $dirbase.'api/apilist.php';
require_once $dirbase.'api/apipost.php';
require_once $dirbase.'api/apidelete.php';
require_once $dirbase.'api/apiget.php';
require_once $dirbase.'api/apiput.php';
