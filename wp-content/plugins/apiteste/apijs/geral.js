listData();
$("#formcadastro").hide();

function interagirbotao() {
    $('button').click(function() {
        var valor = $(this).val();
        var tipo = $(this).attr('tipo')
        if (tipo == 'deletar') {
            $("#adcionarpaciente").show();
            $("#formcadastro").hide();
            deleteData(valor)
        }
        if (tipo == 'editar') {
            $(".rowdependente").remove()
            $('#formcadastro').attr('tipo', 'editar')
            globalid = $(this).val();
            var id = $(this).val();
            getData(id)
            $("#formcadastro").show();
            $("#adcionarpaciente").hide();
        }
    })
}



var globalid;
$('#adcionarpaciente').click(function() {
    $('#adcionarpaciente').hide();
    $("#formcadastro").show();
    $('#formcadastro').attr('tipo', 'adicionar')

    $(".rowdependente").remove();
    $("#myRepeatingFields").append(`<div class="entry inline-form col-xs-12 rowdependente" >
            <input class="form-control" name="fields[]"  type="text" placeholder="Nome" />
            <input class="form-control" name="fields2[]" type="text" placeholder="Data nascimento" />
            <button type="button" class="btn btn-success btn-lg btn-add">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>
        </div>`)
})

$("#formcadastro").on("submit", function(event) {
    var tipo = $("#formcadastro").attr('tipo')
    event.preventDefault();

    var data = $(this).serializeJSON();

    if (tipo == 'editar') {
        editData(data, globalid)
    }
    if (tipo == 'adicionar') {
        addData(data)
    }
});
$.ajaxSetup({
    cache: false
});
$(function() {
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();
        var controlForm = $('#myRepeatingFields:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('');
    }).on('click', '.btn-remove', function(e) {
        e.preventDefault();
        $(this).parents('.entry:first').remove();
        return false;
    });
});