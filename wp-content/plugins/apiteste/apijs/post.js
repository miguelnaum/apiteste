function addData(data){
    $.ajax({
      type: "POST",
      url: "https://dev-apiteste.pantheonsite.io/wp-json/api/paciente/add",
      data: JSON.stringify(data),
      contentType: "application/json; charset=utf-8",        
      dataType: "json",
      cache: false,
      success: function (data, status, jqXHR) {
        $("table > tbody").empty(); 
        listData()
        $( "#formcadastro" ).hide();
        $('#formcadastro')[0].reset();
  
        $("#adcionarpaciente").show();
        
      },
      error: function (jqXHR, status) {
          // error handler
          console.log(jqXHR);
          alert('fail' + status.code);
      }
    });
  }