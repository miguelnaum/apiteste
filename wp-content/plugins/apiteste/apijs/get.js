function getData(id){        
    $(document).ready(function(){
      $.ajax({
        type:"GET",
        dataType: "json",
        url:"https://dev-apiteste.pantheonsite.io/wp-json/api/paciente/"+id,
        success:function(data)
        {
          console.log(data);
          $("#nome").val(data.nome);
          $("#sobrenome").val(data.sobrenome);
          $("#genero").val(data.genero);
          $("#dt-nascimento").val(data.dt_nascimento);
          $("#telefone").val(data.telefone);
          var dependentes = data.dependentes
         
          $.each(dependentes, function(k,v) {   
            console.log(v);
            $("#myRepeatingFields").append(`<div class="entry inline-form col-xs-12 rowdependente">
                      <input class="form-control" name="fields[]" value="`+v.nome+`"  type="text" placeholder="Nome" />
                      <input class="form-control" name="fields2[]" value="`+v.data_nascimento+`"type="text" placeholder="Data nascimento" />
                      <button type="button" class="btn btn-lg btn-remove btn-danger"></button>
                </div>`)
          })
        
        }
      });              
    });
  }