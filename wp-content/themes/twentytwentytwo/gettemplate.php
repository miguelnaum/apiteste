<?php
//Template Name: get
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Bootstrap 101 Template</title>
      <?php wp_head();?>
      <!-- Bootstrap -->
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <!-- Optional theme -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <table class="table">
                  <thead>
                     <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">#</th>
                     </tr>
                  </thead>
                  <tbody>     
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <form id="formcadastro">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Nome</label>
                     <input type="nome" name='nome' class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Nome">
                  </div>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Sobrenome</label>
                     <input  name='sobrenome' class="form-control"  id="sobrenome" placeholder="Sobrenome">
                  </div>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Telefone</label>
                     <input type="nome" name='telefone' class="form-control" id="telefone" aria-describedby="emailHelp" placeholder="Telefone">
                  </div>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Data nascimento</label>
                     <input type="nome" name='dt-nascimento' class="form-control" id="dt-nascimento"  placeholder="Data de nascimento">
                  </div>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Gênero</label>
                     <input type="nome" name='genero' class="form-control" id="genero"  placeholder="Gênero">
                  </div>
                  <label class="control-label" for="ourField">Adicionar dependente</label>
                  <div id="myRepeatingFields">
                  </div>
                  <br>   
                  <button  class="btn btn-primary mt-4 enviar"> Salvar</button>
               </form>
            </div>
         </div>
      </div>
      <a id="adcionarpaciente" class='btn btn-success' >Adicionar paciente</a>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/3.2.1/jquery.serializejson.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <?php wp_footer()?>
   </body>
</html>