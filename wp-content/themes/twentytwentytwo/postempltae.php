<?php
//Template Name: Post


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  </head>
  <body>

  <style>
        .entry:not(:first-of-type)
    {
        margin-top: 10px;
    }
    .glyphicon
    {
        font-size: 12px;
    }
  </style>

  <div class="container">
    <div class="row">
        <div class="col-md-12">
        <form id="formcadastro">

<div class="form-group">
    <label for="exampleInputEmail1">Nome</label>
    <input type="nome" name='nome' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome">
   
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Sobrenome</label>
    <input type="nome" name='sobrenome' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome">
   
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Telefone</label>
    <input type="nome" name='telefone' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome">
   
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Data nascimento</label>
    <input type="nome" name='dt-nascimento' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome">
   
  </div>


  <div class="form-group">
    <label for="exampleInputEmail1">Gênero</label>
    <input type="nome" name='genero' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome">
   
  </div>

  <label class="control-label" for="ourField">Adicionar dependente</label>

     <div id="myRepeatingFields">
         <div class="entry inline-form col-xs-12">
                <input class="form-control" name="fields[]" type="text" placeholder="Nome" />
                <input class="form-control" name="fields2[]" type="text" placeholder="Data nascimento" />
                <button type="button" class="btn btn-success btn-lg btn-add">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
           </div>
      </div> 

<br>   
  <button  class="btn btn-primary mt-4 enviar"> Salvar</button>
</form>

        </div>
    </div>
  </div>





  


  

  


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/3.2.1/jquery.serializejson.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(function()
          {
              $(document).on('click', '.btn-add', function(e)
              {
                  e.preventDefault();
                  var controlForm = $('#myRepeatingFields:first'),
                  currentEntry = $(this).parents('.entry:first'),
                  newEntry = $(currentEntry.clone()).appendTo(controlForm);
                  newEntry.find('input').val('');
                  controlForm.find('.entry:not(:last) .btn-add')
                      .removeClass('btn-add').addClass('btn-remove')
                      .removeClass('btn-success').addClass('btn-danger')
                      .html('');
              }).on('click', '.btn-remove', function(e)
              {
                e.preventDefault();
                $(this).parents('.entry:first').remove();
                return false;
              });
          });
    </script>


    <script>
      $( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      console.log( $( this ).serializeJSON() );
      var data = $( this ).serializeJSON();   
      addData(data)   
      });

      function addData(data){
        $.ajax({
          type: "POST",
          url: "https://dev-apiteste.pantheonsite.io/wp-json/api/postpaciente",
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",        
          dataType: "json",
          cache: false,
          success: function (data, status, jqXHR) {
            window.location.href = "https://dev-apiteste.pantheonsite.io/get/";
          },
          error: function (jqXHR, status) {
              // error handler
              console.log(jqXHR);
              alert('fail' + status.code);
          }
        });
      }

    </script>
</body>
</html>




